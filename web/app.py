from flask import Flask, render_template, request
import os, sys
sys.path.append(r"../../yolo")
from yolo import predictions
# webserver gateway interface
app = Flask(__name__)
BASE_PATH = os.getcwd()
UPLOAD_PATH = os.path.join(BASE_PATH, 'static/upload/')

@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        upload_file = request.files['image_name']
        filename = upload_file.filename
        path_save = os.path.join(UPLOAD_PATH, filename)
        upload_file.save(path_save)
        PATH_H5 = 'static/models/object_detection.h5'
        PATH_PREDICT = 'static/predict/{}'.format(filename)
        model = predictions.Model(path_save, filename, PATH_H5, PATH_PREDICT)
        image, coords = model.object_detection()
        ocr = predictions.OCR(image, coords, filename, PATH_PREDICT)
        ocr.load_image()
        text = ocr.extract_text()
        print(text)
        return render_template('index.html')

    return render_template('index.html')

if __name__=="__main__":
    app.run(debug=True)


#    example
# model = Model(path)
# image, coords = model.object_detection()
# ocr = OCR(image, coords)
# ocr.load_image()
# text = ocr.extract_text()
# print(text)


import numpy as np
import cv2
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras.preprocessing.image import load_img, img_to_array



class Model():

    def __init__(self, path:str):
        self.path = path

    def test_image(self):
        path = './Module1-labeling/images/N9.jpeg'
        image = load_img(path)  #PIL OBJECT
        image = np.array(image, dtype=np.uint8) #8 bit array
        image1 = load_img(path, target_size=(224,224))
        image_arr_224 = img_to_array(image1)/255.0  #Convert into array and normalized output
        h,w,d = image.shape
        #plt.imshow(image)
        #plt.show()
        test_arr = image_arr_224.reshape(1,224,224,3)
        self.make_predictions(test_arr, h, w, image)

    def make_predictions(self, test_arr, h, w, image):
        model = tf.keras.models.load_model('./models/object_detection.h5')
        coords = model.predict(test_arr)
        image, coords = self.denormalize(coords, h, w, image)
        return image, coords

    def denormalize(self, coords, h, w, image):
        denorm = np.array([w,w,h,h])
        coords = coords * denorm
        coords = coords.astype(np.int32)
        image, coords = self.draw_bounding_box(coords, image)
        return image, coords

    def draw_bounding_box(self, coords, image):
        xmin, xmax, ymin, ymax = coords[0]
        pt1 = (xmin, ymin)
        pt2 = (xmax, ymax)
        cv2.rectangle(image, pt1,pt2,(0,255,0))
        return image, coords

    def object_detection(self):
        image = load_img(self.path)  # PIL OBJECT
        image = np.array(image, dtype=np.uint8)  # 8 bit array
        image1 = load_img(path, target_size=(224, 224))
        image_arr_224 = img_to_array(image1) / 255.0  # Convert into array and normalized output
        h, w, d = image.shape
        test_arr = image_arr_224.reshape(1, 224, 224, 3)
        image, coords = self.make_predictions(test_arr, h, w, image)
        return image, coords



path = './Module1-labeling/images/N147.jpeg'
model = Model(path)
image, coords = model.object_detection()
plt.imshow(image)
plt.show()
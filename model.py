
import cv2
import tensorflow as tf
import pandas as pd
import xml.etree.ElementTree as xet
from glob import glob
import numpy as np
import matplotlib.pyplot as plt
import os
from sklearn.model_selection import train_test_split
from tensorflow.keras.preprocessing.image import load_img, img_to_array
from tensorflow.keras.applications import MobileNetV2, InceptionV3, InceptionResNetV2
from tensorflow.keras.layers import Dense, Dropout, Flatten, Input
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import TensorBoard
pd.options.mode.chained_assignment = None

class XmlToCsv:

    def __init__(self, images: list):
        self.images = images

    def info_images(self):
        labels_dict = dict(filepath=[], xmin=[], xmax=[], ymin=[], ymax=[])
        for filename in self.images:
            #filename = self.images[0]
            info = xet.parse(filename)
            root = info.getroot()
            member_object = root.find('object')
            labels_info = member_object.find('bndbox')
            xmin = int(labels_info.find('xmin').text)
            xmax = int(labels_info.find('xmax').text)
            ymin = int(labels_info.find('ymin').text)
            ymax = int(labels_info.find('ymax').text)
            #print(xmin, xmax, ymin, ymax)
            labels_dict['filepath'].append(filename)
            labels_dict['xmin'].append(xmin)
            labels_dict['xmax'].append(xmax)
            labels_dict['ymin'].append(ymin)
            labels_dict['ymax'].append(ymax)
        return labels_dict

    def convertToCsv(self):
        data_dict = self.info_images()
        dataframe = pd.DataFrame(data_dict)
        dataframe.to_csv('labels.csv', index=False)
        return dataframe




class ObjectDetection():

    def __init__(self, filepath:str):
        self.filepath = filepath

    def open_csv(self, df):
        for i in range(len(df['filepath'])):
            filename = df['filepath'][i]
            getFileName = self.get_file_name(filename)
            df['filepath'][i] = getFileName
        lista = list(df['filepath'])
        # self.verify_image(lista)
        return lista


    def data(self):
        df = pd.read_csv(self.filepath)
        self.open_csv(df)
        return df

    def get_file_name(self, filename):
        filename_image = xet.parse(filename).getroot().find('filename').text
        filepath_image = os.path.join('./Module1-labeling/images', filename_image)
        return filepath_image

    def verify_image(self, image_path):
        file_path = image_path[0]
        img = cv2.imread(file_path)
        cv2.rectangle(img, (119, 308), (328, 422), (0, 255, 0), 3)
        cv2.namedWindow('example', cv2.WINDOW_NORMAL)
        cv2.imshow('example', img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()



class DataPreprocessing():

    def processing(dataframe):
        data = []
        output = []
        imagepath = list(dataframe['filepath'])
        for index in range(len(imagepath)):
            labels = dataframe.iloc[:,1:].values
            img_arr = cv2.imread(imagepath[index])
            h,w,d = img_arr.shape
            # preprocessing
            load_image = load_img(imagepath[index], target_size=(224,224))
            load_image_arr = img_to_array(load_image)
            norm_load_image_arr = load_image_arr/255.0  #normalizations
            # normalization to labels
            xmin,xmax, ymin, ymax =  labels[index]
            nxmin, nxmax = xmin/w, xmax/w
            nymin, nymax = xmin/h, xmax/h
            label_norm = (nxmin, nxmax, nymin, nymax)   #normalized output
            data.append(norm_load_image_arr)
            output.append(label_norm)
        X = np.array(data,dtype=np.float32)
        Y = np.array(output,dtype=np.float32)
        x_train, x_test, y_train, y_test = train_test_split(X,Y, train_size=0.8, random_state=0)
        DataPreprocessing.training(x_train,x_test,y_train,y_test)

    def training(x_train, x_test, y_train, y_test):
        DeepLearningModel.model_training(x_train, x_test, y_train, y_test)

class DeepLearningModel():

    def model():
        inception_resnet = InceptionResNetV2(weights="imagenet", include_top=False, input_tensor=Input(shape=(224,224,3)))
        inception_resnet.trainable = False
        headmodel = inception_resnet.output
        headmodel = Flatten()(headmodel)
        headmodel = Dense(500, activation='relu')(headmodel)
        headmodel = Dense(250, activation='relu')(headmodel)
        headmodel = Dense(4, activation='sigmoid')(headmodel)
        model = Model(inputs = inception_resnet.input, outputs = headmodel)
        model.compile(loss='mse', optimizer=tf.keras.optimizers.Adam(learning_rate=1e-4))
        return model

    def model_training(x_train, x_test, y_train, y_test):
        model = DeepLearningModel.model()
        tfb = TensorBoard('object_detection')
        history = model.fit(x = x_train, y=y_train, batch_size=10, epochs=200, validation_data=(x_test, y_test),
                            callbacks=[tfb])
        model.save('models/object_detection.h5')






path_images = sorted(glob('Module1-labeling/images/*.xml'))
convert = XmlToCsv(path_images)
convert.info_images()
convert.convertToCsv()
detection = ObjectDetection('labels.csv')
data = detection.data()
preprocesamiento = DataPreprocessing.processing(data)
